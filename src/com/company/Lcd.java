package com.company;
import java.util.ArrayList;
public class Lcd {
    private String Status;
    private int Volume;
    private int Brightness;
    private String Cable;
    private int cableOption=1;


    public String turnOn(){
        return Status="On";
    }
    public String turnOff(){
        return Status="Off";
    }
    public String Freeze(){
        return  Status="Freeze";
    }
    public  int volumeUp(){
        return Volume++;
    }
    public int volumeDown(){
        return Volume--;
    }
    public  void setVolume(int Volume){
        this.Volume = Volume;
    }
    public int brightnessUp(){
        return Brightness++;
    }
    public int brightnessDown(){
        return Brightness--;
    }
    public void  setBrightness(int Brightness){
        this.Brightness = Brightness;
    }
    public void cableUp() {
        this.cableOption++;
    }

    public void cableDown() {
        this.cableOption--;
    }

    public void LCDcable() {
        switch (cableOption) {
            case 1:
                Cable = "DVI";
                break;
            case 2:
                Cable = "VGA";
                break;
            case 3:
                Cable = "HDMI";
                break;
            default:
                Cable = "Pilihan cable tidak tersedia";
                break;
        }
    }
        public void setCable() {
            this.Cable = Cable;
        }


        public void displayMassage(){
        System.out.println("Status     = "+Status);
        System.out.println("Volume     = "+Volume);
        System.out.println("Brightness = "+Brightness);
        System.out.println("Cable      = "+ Cable);
    }
}

