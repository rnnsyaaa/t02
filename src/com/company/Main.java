package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println("----------Set Up LCD----------");
        Lcd LCD1 = new Lcd();
        LCD1.turnOn();
        LCD1.turnOff();
        LCD1.Freeze();
        LCD1.setVolume(50);
        LCD1.volumeDown();
        LCD1.setBrightness(40);
        LCD1.brightnessDown();
        LCD1.LCDcable();
        LCD1.displayMassage();
    }
}
